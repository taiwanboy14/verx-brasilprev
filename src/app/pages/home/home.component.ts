import { Component, ElementRef, OnInit } from '@angular/core';

import { PokemonCardsService } from 'src/app/services/pokemon-cards.service';

import { PokemonCard } from 'src/app/interfaces/PokemonCard';
import { ServiceReturnCards } from 'src/app/interfaces/ServiceReturnCards';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  
  items: PokemonCard[];
  search: boolean = false;
  term: string = '';

  constructor(
    private pokemonCardsService: PokemonCardsService,
    private el: ElementRef
  ) { }

  ngOnInit(): void {
    this.fnSearch();
  }

  fnInputKeyup(evt): void {
    evt.stopPropagation();
    this.term = evt.target.value;
  }

  fnSearch(click = null) {
    if (click && this.term) this.search = true;
    this.pokemonCardsService.getCards(this.term)
    .subscribe(allCards => {
      this.items = (<ServiceReturnCards>allCards).cards.sort((a, b) => a.name > b.name ? 1 : -1);
    });
  }
  fnClear() {
    this.search = false;
    this.term = '';
    this.el.nativeElement.querySelector('#input-search').value = null;
    this.fnSearch();
  }
}
