import { Component, OnInit } from '@angular/core';

import { ActivatedRoute } from '@angular/router';
import { PokemonCardsService } from 'src/app/services/pokemon-cards.service';

import { PokemonCard } from 'src/app/interfaces/PokemonCard';
import { ServiceReturnCard } from 'src/app/interfaces/ServiceReturnCards';

@Component({
  selector: 'app-details',
  templateUrl: './details.component.html',
  styleUrls: ['./details.component.scss']
})
export class DetailsComponent implements OnInit {

  constructor(
    private activatedRoute: ActivatedRoute,
    private pokemonCardsService: PokemonCardsService
  ) { }

  id: string;
  card: PokemonCard = null;

  ngOnInit(): void {
    this.getCard();
  }
  
  getCard() {
    this.id = this.activatedRoute.snapshot.params.id;
    this.pokemonCardsService.getCardById(this.id)
      .subscribe(card => {
        console.log(card);
        (this.card = (<ServiceReturnCard>card).card);
      });
  }
}