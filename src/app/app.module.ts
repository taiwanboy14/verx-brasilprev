import { BrowserModule } from '@angular/platform-browser';
import { CommonModule } from '@angular/common'; 
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';

import { HomeComponent } from './pages/home/home.component';
import { DetailsComponent } from './pages/details/details.component';

import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { TypesListComponent } from './components/types-list/types-list.component';
import { CardItemComponent } from './components/card-item/card-item.component';
import { TagComponent } from './components/tag/tag.component';
import { PokemonEnergyComponent } from './components/pokemon-energy/pokemon-energy.component';
import { PokemonAttackItemComponent } from './components/pokemon-attack-item/pokemon-attack-item.component';

@NgModule({
  declarations: [
    // Pages
    HomeComponent,
    DetailsComponent,
    
    // Components
    AppComponent,
    CardItemComponent,
    HeaderComponent,
    FooterComponent,
    TagComponent,
    TypesListComponent,
    PokemonEnergyComponent,
    PokemonAttackItemComponent,
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    CommonModule,
    HttpClientModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
