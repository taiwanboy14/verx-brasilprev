import { Component, Input, OnInit } from '@angular/core';
import { PokemonCard } from 'src/app/interfaces/PokemonCard';

@Component({
  selector: 'app-card-item',
  templateUrl: './card-item.component.html',
  styleUrls: ['./card-item.component.scss']
})
export class CardItemComponent implements OnInit {

  @Input() item: PokemonCard;
  maxDamage: number = 0;

  constructor() { }

  ngOnInit(): void {}
}
