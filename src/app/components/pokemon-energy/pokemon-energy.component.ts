import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-energy',
  templateUrl: './pokemon-energy.component.html',
  styleUrls: ['./pokemon-energy.component.scss']
})
export class PokemonEnergyComponent implements OnInit {

  @Input() type: string;

  constructor() { }

  ngOnInit(): void {
  }

}
