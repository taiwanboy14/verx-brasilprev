import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonEnergyComponent } from './pokemon-energy.component';

describe('PokemonEnergyComponent', () => {
  let component: PokemonEnergyComponent;
  let fixture: ComponentFixture<PokemonEnergyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonEnergyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonEnergyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
