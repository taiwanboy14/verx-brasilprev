import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-types-list',
  templateUrl: './types-list.component.html',
  styleUrls: ['./types-list.component.scss']
})
export class TypesListComponent implements OnInit {

  @Input() types: string[];
  list: string[];

  constructor() { }

  ngOnInit(): void {
    if (this.types && this.types.length) {
      this.list = this.types;
    } else {
      this.list = ['no-pkmn'];
    }
  }

}
