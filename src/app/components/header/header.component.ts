import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  
  @Input() fixed: boolean = false;
  // show: boolean = false;
  // term: string = '';

  constructor() {
    
  }

  ngOnInit(): void {
    if (this.fixed) {
      let html = document.querySelector('html');
      if (!html.classList.contains('has-navbar-fixed-top'))
        html.classList.add('has-navbar-fixed-top');
    }
  }

  // showMenu() {
  //   this.show = !this.show;
  // }

  // fnSearch() {
  //   console.log(this.term);
  // }
}
