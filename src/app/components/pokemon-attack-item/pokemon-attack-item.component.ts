import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pokemon-attack-item',
  templateUrl: './pokemon-attack-item.component.html',
  styleUrls: ['./pokemon-attack-item.component.scss']
})
export class PokemonAttackItemComponent implements OnInit {

  @Input() atk: any;
  @Input() types: string[];

  constructor() { }

  ngOnInit(): void {
    console.log(this.atk, this.types);
  }
}
