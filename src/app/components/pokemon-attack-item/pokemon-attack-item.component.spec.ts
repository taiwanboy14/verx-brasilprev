import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PokemonAttackItemComponent } from './pokemon-attack-item.component';

describe('PokemonAttackItemComponent', () => {
  let component: PokemonAttackItemComponent;
  let fixture: ComponentFixture<PokemonAttackItemComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PokemonAttackItemComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PokemonAttackItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
