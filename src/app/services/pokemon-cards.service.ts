import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';

const API = 'https://api.pokemontcg.io/v1/';

@Injectable({
  providedIn: 'root'
})
export class PokemonCardsService {
  constructor(private http: HttpClient) { }

  getCards(name: string = null) {
    if(name)
      return this.http.get(`${API}cards?name=${name}`);
    return this.http.get(`${API}cards`);
  }

  getCardById(id) {
    return this.http.get(`${API}cards/${id}`);
  }
}