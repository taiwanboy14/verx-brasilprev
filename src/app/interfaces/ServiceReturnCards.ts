import { PokemonCard } from './PokemonCard';

export interface ServiceReturnCard {
  card: PokemonCard;
}

export interface ServiceReturnCards {
  cards: PokemonCard[];
}